package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testklasse voor de ontwikkeling van de klasse Punt.
 * De klasse Punt bevat de x en y coordinaten van een punt.
 */
public class PuntTest {
    private Punt pOne;
    private Punt pTwo;
    private Punt pThree;
    private Punt pFour;

    @BeforeEach
    public void setUp() {
        pOne = new Punt(1, 2);
        pTwo = new Punt(2, 1);
        pThree = new Punt(1, 1);
        pFour = new Punt(3, 2);
    }

    /**
     * Test de equals methode.
     * Vergelijking tussen twee Punt objecten (coordinaten moeten overeenstemmen).
     */
    @Test
    public void testEqual() {
        assertEquals(pOne, new Punt(1, 2), "constructor (x,y) moet gelijke waarde geven");
        assertNotEquals(pOne, pTwo, "Punten moeten verschillend zijn");
        assertNotEquals(pOne, "1, 2", "Punt mag geen string zijn");
    }

    /**
     * Test de hashcode methode.
     * De hashcode is op basis van beide coordinaten.
     */
    @Test
    public void testHashcode() {
        int codeOne = pOne.hashCode();
        int codeTwo = pTwo.hashCode();
        assertNotEquals(codeOne , codeTwo, "Hashcode must be different");
        assertEquals(codeOne , pOne.hashCode(),"Hashcode must be equal");
    }

    /**
     * Test de methode distance.
     * Dit is de afstand tussen twee punten.
     */
    @Test
    public void testDistance() {
        double dist = Math.sqrt(5);
        assertEquals( dist, pThree.distance(pFour), "Distance must be sqrt(5)");

        dist = Math.sqrt(2);
        assertEquals( dist, pOne.distance(pTwo), "Distance must be sqrt(2)");
    }

    /**
     * Test de methode distanceToCenter.
     * Dit is de afstand van het punt tot het nulpunt (coordinaten 0,0).
     */
    @Test
    public void testDistanceToCenter() {
        double distOne = pOne.distanceToCenter();
        double distTwo = pTwo.distanceToCenter();
        assertEquals( distOne, Math.sqrt(5), "Distance must be sqrt(5)");
        assertEquals(distOne ,distTwo,"Distances must be equal");
    }

    /**
     * Test de compare methode.
     * Deze methode vergelijkt de afstanden tot het nulpunt.
     */
    @Test
    public void testCompare() {
        assertEquals(0, pOne.compareTo(pTwo) ,"Distance P1 must be same as P2");
        assertTrue( pOne.compareTo(pFour) < 0,"Result must be < 0");
        assertTrue(pOne.compareTo(pThree) > 0,"Result must be > 0");
    }
}